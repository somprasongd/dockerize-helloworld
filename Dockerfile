FROM node:6.9.1
LABEL maintainer "Somprasong Damyos"
RUN useradd -ms /bin/bash admin
USER admin
WORKDIR /nodeapp
COPY app /nodeapp
CMD ["node", "server.js"]
